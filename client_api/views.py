from django.shortcuts import render
from django.core.files import File
from django.http import HttpResponse
import requests
import json
#API for vendor upload
def client_insertion(request):
    file1 = open('/home/omkar/Downloads/client_id.csv', 'r')
    already_exist = open("/home/omkar/Downloads/already_exist.csv","w")
    not_present_in_master = open("/home/omkar/Downloads/not_present_in_master.csv","w")
    successful = open("/home/omkar/Downloads/successful.csv","w")

    
    headers_for_client = {"content-type": "application/json","token":"b8a2d46x32Gb29fm26fee5M658tD57mf","versionnumber":"v1"}
    url = 'http://xbmasterapi.xbees.in/expose/client/dropdown/list'
    r = requests.post(url,headers=headers_for_client)

    list1=r.json()['data']
    client_ids=[]
    not_in_master_count=0
    for element in range(len(list1)):
        client_ids.append(list1[element]['ClientMasterID'])

    if file1.mode == 'r':
        contents = list(file1.readlines())

        url = 'http://autoallocation-api.xbees.in/api/addNewClientCutoff'
        headers_for_cutoff = {"authkey":"xB84JJ89Hd25"}

        for i in range(len(contents)):
            contents[i]=int(contents[i].strip('\n'))
            if contents[i] in client_ids:
                payload = {'clientid': contents[i]}
                r = requests.post(url,data=json.dumps(payload),headers=headers_for_cutoff)
                if str(r.json()['ReturnMessage'])=="successful":
                    successful.write(str(contents[i])+','+str(r.json()['ReturnMessage']))
                    successful.write('\n')
                else:
                    already_exist.write(str(contents[i])+','+str(r.json()['ReturnMessage']))
                    already_exist.write('\n')
            else:
                not_in_master_count+=1
                not_present_in_master.write(str(contents[i])+','+"ClientID not present in Master")
                not_present_in_master.write('\n')
        already_exist.close() 
        not_present_in_master.close() 
        successful.close() 

    return HttpResponse("ClientIDs insertion completed.")

    


