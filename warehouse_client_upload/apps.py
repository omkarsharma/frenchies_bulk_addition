from django.apps import AppConfig


class WarehouseClientUploadConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'warehouse_client_upload'
