from django.shortcuts import render
from django.core.files import File
from django.http import HttpResponse
import requests
import json
from django.db import connection
import pandas as pd


def upload_client(request):
    df = pd.read_excel(
        r'/home/omkar/Downloads/FM_Warehouse.xlsx', sheet_name='Sheet1')
    
    if "ClientID" not in df.columns:
        return HttpResponse("ClientID Key not present in File.")
    elif "HubName" not in df.columns:
        return HttpResponse("HubName Key not present in File.")
    elif "Address" not in df.columns:
        return HttpResponse("Address Key not present in File.")

    column_names = ["ClientID", "HubName", "Address"]
    df = df.reindex(columns=column_names)
   
    not_inserted_clientid = open(
        "/home/omkar/Downloads/not_inserted_clientid.csv", "w")
    hub_name_not_match = open(
        "/home/omkar/Downloads/hub_name_not_match.csv", "w")

    already_exist = open("/home/omkar/Downloads/already_exist.csv", "w")
    successful = open("/home/omkar/Downloads/successful.csv", "w")

    #   client_ids and client names
    headers_for_client = {"content-type": "application/json",
                          "token": "b8a2d46x32Gb29fm26fee5M658tD57mf", "versionnumber": "v1"}
    url = 'http://xbmasterapi.xbees.in/expose/client/dropdown/list'
    r = requests.post(url, headers=headers_for_client)
    client_data = r.json()['data']

    #   hub_ids and hub names
    headers_for_hubids = {"content-type": "application/json",
                          "token": "b8a2d46x32Gb29fm26fee5M658tD57mf", "versionnumber": "v1"}
    url = 'http://xbmasterapi.xbees.in/expose/client/dropdown/list'
    r = requests.post(url, headers=headers_for_client)
    client_data = r.json()['data']




    headers_for_hubids = {"content-type": "application/json",
                          "token": "b8a2d46x32Gb29fm26fee5M658tD57mf", "versionnumber": "v1"}
    url = 'http://xbmasterapi.xbees.in/expose/hubservicecenter/get/byHubzoneid'
    hubid_payload = {"hubzoneids": [1, 2, 3, 4, 5, 6, 7]}
    r = requests.post(url, data=json.dumps(
        hubid_payload), headers=headers_for_hubids)
    hub_data = r.json()['data']

    url = 'http://autoallocation-api.xbees.in/api/addClientCutoffMaster'
    headers_for_cutoff = {"authkey": "xB84JJ89Hd25"}
    for i in range(0, len(df.index)):
        temp = df.iloc[i]
        client_flag = True
        for k in range(len(client_data)):
            if client_data[k]["ClientMasterID"] == int(temp.ClientID):
                client_flag = False
                hub_flag = True
                for j in range(len(hub_data)):
                    if hub_data[j]["HubName"] == temp.HubName:
                        hub_flag = False
                        payload = {"clientid": int(temp.ClientID),
                                   "time": "0600",
                                   "pickuptype": "warehouse",
                                   "whid": 0,
                                   "hubid": hub_data[j]["HubID"],
                                   "servicetype": "Standard",
                                   "pickuplocationcode": None,
                                   "address": temp.Address,
                                   "isthirtyminute": False,
                                   "groupingid": 4,
                                   "createdby": "omkar.sharma@xpressbees.com",
                                   }
                        r = requests.post(url, data=json.dumps(
                            payload), headers=headers_for_cutoff)
                        if str(r.json()['ReturnMessage']) == "successful":
                            successful.write(str(
                                int(temp.ClientID))+','+temp.HubName + ','+str(r.json()['ReturnMessage']))
                            successful.write('\n')
                        else:
                            already_exist.write(str(
                                int(temp.ClientID))+','+temp.HubName + ','+str(r.json()['ReturnMessage']))
                            already_exist.write('\n')
                if hub_flag:
                    hub_name_not_match.write(str(
                                int(temp.ClientID))+','+temp.HubName)
                    hub_name_not_match.write('\n')

        if client_flag:
            not_inserted_clientid.write(str(int(temp.ClientID)))
            not_inserted_clientid.write('\n')

    return HttpResponse("ClientIDs insertion completed.")



def sep(request):
    df = pd.read_excel(
        r'/home/omkar/Downloads/sep_by_commas.xlsx', sheet_name='Sheet1')
    column_names = ["ClientID"]
    df = df.reindex(columns=column_names)
    sep_comma= open("/home/omkar/Downloads/sep_comma.csv", "w")
    string=""
    for i in range(0, len(df.index)):
        temp = df.iloc[i]
        string +=str(int(temp.ClientID))+','
    print(string)
    return HttpResponse("Sepration is completed.")
